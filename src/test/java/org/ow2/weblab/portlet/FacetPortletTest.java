/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet;

import java.io.File;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.jaxb.WebLabMarshaller;
import org.ow2.weblab.core.model.ComposedQuery;
import org.ow2.weblab.core.model.Operator;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.core.model.StringQuery;
import org.ow2.weblab.core.model.retrieval.WRetrievalAnnotator;
import org.ow2.weblab.portlet.business.bean.FacetBundle;
import org.ow2.weblab.portlet.business.bean.FacetConfig;
import org.ow2.weblab.portlet.business.service.FacetBusinessService;
import org.ow2.weblab.portlet.business.service.FacetBusinessServiceSPARQL;
import org.ow2.weblab.portlet.business.service.IFacetBusinessService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class FacetPortletTest {

	private static final String TEST_FILE = "src/test/resources/facetSample.xml";
	//private static final String TEST_FILE = "src/test/resources/test.xml";
	private IFacetBusinessService service;
	private IFacetBusinessService serviceSPARQL;
	private final Log log = LogFactory.getLog(this.getClass());

	@Before
	public void init() {
		this.service = new FacetBusinessService();
		this.serviceSPARQL = new FacetBusinessServiceSPARQL();

		try (final ClassPathXmlApplicationContext factory = new ClassPathXmlApplicationContext(FacetConfig.DEFAULT_CONF_FILE)) {
			this.service.setConfig(factory.getBean(FacetConfig.DEFAULT_BEAN_NAME, FacetConfig.class));
			this.serviceSPARQL.setConfig(factory.getBean(FacetConfig.DEFAULT_BEAN_NAME, FacetConfig.class));
		}

	}

	@Test
	public void testResultSetProcess() throws WebLabCheckedException {
		final ResultSet rSet = new WebLabMarshaller().unmarshal(new File(FacetPortletTest.TEST_FILE), ResultSet.class);

		long t = System.currentTimeMillis();
		FacetBundle bundle = this.serviceSPARQL.processResultSet(rSet);
		this.log.info("facets retrieved SPARQL in "+(System.currentTimeMillis()-t)+" ms");


		bundle = this.service.processResultSet(rSet);
		this.log.info("facets retrieved BJH in "+(System.currentTimeMillis()-t)+" ms");

		t = System.currentTimeMillis();
		this.serviceSPARQL.processResultSet(rSet);
		this.log.info("facets retrieved SPARQL in "+(System.currentTimeMillis()-t)+" ms");

		bundle = this.service.processResultSet(rSet);
		this.log.info("facets retrieved BJH in "+(System.currentTimeMillis()-t)+" ms");

		Assert.assertFalse(bundle.isEmpty());
		Assert.assertEquals(34, bundle.getFQueryByURIMap().size());
	}

	@Test
	public void testPrepareQuery() {
		final StringQuery query = WebLabResourceFactory.createResource("test", "query0" + System.currentTimeMillis(), StringQuery.class);
		query.setRequest("*:*");

		final StringQuery facet = WebLabResourceFactory.createResource("test", "query1" + System.currentTimeMillis(), StringQuery.class);
		facet.setRequest("http\\://www.gutenberg.org/*");

		final ComposedQuery cQuery = WebLabResourceFactory.createResource("test", "query3" + System.currentTimeMillis(), ComposedQuery.class);
		cQuery.setOperator(Operator.AND);

		cQuery.getQuery().add(query);
		cQuery.getQuery().add(facet);

		final WRetrievalAnnotator wra = new WRetrievalAnnotator(cQuery);
		wra.writeExpressedWith(FacetBusinessService.FACET_SUGGESTION);
		wra.writeNumberOfResults(Integer.valueOf(10));
		wra.writeQueryOffset(Integer.valueOf(0));

		final long tic = System.currentTimeMillis();

		//final Query rQuery = this.service.prepareQuery(cQuery);

		final long toc = System.currentTimeMillis();

		LogFactory.getLog(this.getClass()).info("duration = " + (toc - tic) + "ms");

		//final JenaResourceHelper rhlpr = new JenaResourceHelper(rQuery);
		//final Set<String> subj = rhlpr.getSubjsOnPredLit(WebLabRetrieval.IS_EXPRESSED_WITH, FacetBusinessService.FACET_SUGGESTION);
		//Assert.assertTrue((subj == null) || (subj.size() == 0));
	}
}
