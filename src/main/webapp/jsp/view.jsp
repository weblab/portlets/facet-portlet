<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<portlet:defineObjects />

<fmt:setLocale value="${pageContext.request.locale}" />
<fmt:setBundle basename="facet_portlet" />

<portlet:resourceURL id="facets" var="facetsURL"/>



		<div class="contenu_portlet" id="facetPortletDiv">
			<!-- to be loaded by AJAX call... -->
			<div id="facetLoading">
			
			</div>
		</div>
		<script>
			jQuery("#facetPortletDiv").load("${facetsURL}");
		</script>

