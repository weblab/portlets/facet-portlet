<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<portlet:defineObjects />

<fmt:setLocale value="${pageContext.request.locale}" />
<fmt:setBundle basename="facet_portlet" />

<c:choose>
	<c:when test="${empty facet_cats }">
			<div class="portlet-msg-info"><fmt:message key="portlet.no_results" /></div>
	</c:when>
	<c:otherwise>


		<!-- Facet filters -->
		<ul class="facet">
			<!-- Specific filters for dates -->
			<span class="facetLabel" onclick="jQuery('ul#facet_date').toggle();"><fmt:message key="entity.dateEntity" /></span>
			<ul id="facet_date" class="facetValue">
				<li class="facetValue">
					<a href="<portlet:actionURL name="add_day_facet" />" title="<fmt:message key="last.day" />"><fmt:message key="last.day"/></a>
				</li>
				<li class="facetValue">
					<a href="<portlet:actionURL name="add_week_facet" />" title="<fmt:message key="last.week" />"><fmt:message key="last.week"/></a>
				</li>
				<li class="facetValue">
					<a href="<portlet:actionURL name="add_month_facet" />" title="<fmt:message key="last.month" />"><fmt:message key="last.month"/></a>
				</li>
			</ul>
			
			<!-- Dynamic facet values -->
			<c:forEach var="facet_uri" items="${facet_order}" varStatus="facetStatus">
				<c:if test="${not empty facet_cats[facet_uri]}">
					<span class="facetLabel" onclick="jQuery('ul#facet_${fn:trim(facet_cats[facet_uri].label)}').toggle();"><fmt:message key="${facet_cats[facet_uri].label}" /></span>
					<ul id="facet_${fn:trim(facet_cats[facet_uri].label)}" class="facetValue">
						<c:forEach var="facet" items="${facet_cats[facet_uri].facets}" varStatus="facetValueStatus">
							<li class="facetValue">
								<portlet:actionURL var="add_filter_url" name="add_facet">
									<portlet:param name="facet_query_uri" value="${facet.query.uri}"></portlet:param>
								</portlet:actionURL>
								<portlet:actionURL var="remove_filter_url" name="remove_facet">
									<portlet:param name="facet_query_uri" value="${facet.query.uri}"></portlet:param>
								</portlet:actionURL>
								<a href="${add_filter_url}" title="<fmt:message key="title.addToIncludedValues"/>" >
								${facet.label} - (${facet.countAnd})
								</a>
								<!-- <a class="includeFilter" href="${add_filter_url}" title="<fmt:message key="title.addToIncludedValues"/> (${facet.countAnd})" ><img src="<c:url value='/images/bullet_add.png'/>"/></a> -->
								<a class="excludeFilter" href="${remove_filter_url}" title="<fmt:message key="title.addToExcludedValues"/> (${facet.countNot})" ><img src="<c:url value='/images/bullet_delete.png'/>"/></a>
							</li>
						</c:forEach>
					</ul>
				</c:if>
			</c:forEach>
		</ul>
	</c:otherwise>
</c:choose>