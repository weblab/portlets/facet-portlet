/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.business.service;

import java.net.URI;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.ontologies.WebLabRetrieval;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.helper.impl.JenaResourceHelper;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.ComposedQuery;
import org.ow2.weblab.core.model.Operator;
import org.ow2.weblab.core.model.Query;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.core.model.StringQuery;
import org.ow2.weblab.core.model.retrieval.WRetrievalAnnotator;
import org.ow2.weblab.core.services.searcher.SearchArgs;
import org.ow2.weblab.portlet.business.bean.Facet;
import org.ow2.weblab.portlet.business.bean.FacetBundle;
import org.ow2.weblab.portlet.business.bean.FacetCategory;
import org.ow2.weblab.portlet.business.bean.FacetConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * The Facet portlet business service implementation
 *
 * @author gdupont, emilienbondu
 *
 */
@Service()
public class FacetBusinessService implements IFacetBusinessService {

	public static final String FACET_SUGGESTION = "FacetSuggestion";

	// attributes
	private Log logger = LogFactory.getLog(FacetBusinessService.class);

	@Autowired
	private FacetConfig config;

	// methods
	@Override
	public SearchArgs createSearchArgForSelectedFacet(FacetBundle facets,
			String facetQueryURI, Operator operator) {
		Query facetQueryToLaunch = facets.getFQueryByURIMap()
				.get(facetQueryURI);

		// remove annotation that tell the query came from facet suggestion
		// (it's now a query selected by user)
		facetQueryToLaunch = this.prepareQuery(facetQueryToLaunch);

		// TODO launch the right event to search portlet
		// create args for search service
		final SearchArgs args = new SearchArgs();
		args.setLimit(Integer.valueOf(10));
		args.setOffset(Integer.valueOf(0));
		args.setQuery(facetQueryToLaunch);
		return args;
	}

	@Override
	public SearchArgs createSearchArgsForDates(FacetBundle facets,
			DATES_RANGE range) {
		ComposedQuery query = new ComposedQuery();
		query.setUri("http://facet_"+UUID.randomUUID().toString());
		query.setOperator(Operator.AND);
		query.getQuery().add(facets.getOrginalQuery());

		StringQuery dateQuery= new StringQuery();
		GregorianCalendar today = new GregorianCalendar();
		//Compute the end date of search
		String end = DATE_FORMAT.format(today.getTime());

		//Then reset it to midnight
		today.set(Calendar.HOUR_OF_DAY, 23);
		today.set(Calendar.MINUTE, 59);
		today.set(Calendar.SECOND, 59);
		today.set(Calendar.MILLISECOND, 0);

		//Prepare the query in SolR format
		StringBuffer queryBuffer = new StringBuffer("hasGatheringDate:[ ");

		switch(range) {
		case DAY:
			GregorianCalendar yesterday = today;
			//This way, we get data from yesterday to now
			yesterday.add(Calendar.DAY_OF_MONTH, -2);
			queryBuffer.append(DATE_FORMAT.format(yesterday.getTime()));
			break;

		case WEEK:
			GregorianCalendar lastWeek = today;
			lastWeek.add(Calendar.WEEK_OF_YEAR, -1);
			queryBuffer.append(DATE_FORMAT.format(lastWeek.getTime()));
			break;

		case MONTH:
			GregorianCalendar lastMonth = today;
			lastMonth.add(Calendar.MONTH, -1);
			queryBuffer.append(DATE_FORMAT.format(lastMonth.getTime()));
			break;
		}

		queryBuffer.append(" TO " + end + " ]");
		dateQuery.setRequest(queryBuffer.toString());
		dateQuery.setUri("http://facet_"+UUID.randomUUID().toString());

		query.getQuery().add(dateQuery);

		final SearchArgs args = new SearchArgs();
		args.setLimit(Integer.valueOf(10));
		args.setOffset(Integer.valueOf(0));
		args.setQuery(query);

		return args;
	}

	public Query prepareQuery(final Query q) {
		if (q.getAnnotation().size() > 0) {
			for (final Annotation a : q.getAnnotation()) {
				new WRetrievalAnnotator(URI.create(q.getUri()), a).writeExpressedWith(null);
			}
		}
		for (final Query query : ResourceUtil.getSelectedSubResources(q,
				Query.class)) {
			this.prepareQuery(query);
		}
		return q;
	}

	/**
	 * Processing a ResultSet to extract the facet and related information and
	 * produce a valid FacetBundle.
	 *
	 * @param receveidResultSet The received result set
	 * @return The Facet Bundle Bean
	 * @throws WebLabCheckedException If there is not a single facet received, or no valid facet at the end
	 */
	@Override
	public FacetBundle processResultSet(final ResultSet receveidResultSet)
			throws WebLabCheckedException {
		this.logger.debug("Entering processResultSet for "
				+ receveidResultSet.getUri() + ".");
		// getting facet queries
		final JenaResourceHelper hlpr = new JenaResourceHelper(
				receveidResultSet);

		final Set<Query> facetQueries = this.filterResources(receveidResultSet,
				hlpr.getSubjsOnPredLit(WebLabRetrieval.IS_EXPRESSED_WITH,
						FACET_SUGGESTION), Query.class);

		final FacetBundle bundle = new FacetBundle();
		if (facetQueries.size() == 0) {
			throw new WebLabCheckedException(
					"No facet queries received in last resultSet. Cleaning old ones.");
		}
		this.logger.info(facetQueries.size() + " facet queries received.");

		for (final Query q : facetQueries) {
			if (!(q instanceof ComposedQuery)) {
				this.logger.warn("One of the facet query [" + q.getUri()
						+ "] received isn't a composedQuery. Ignoring it.");
				continue;
			}
			final ComposedQuery facetQuery = (ComposedQuery) q;
			// check the link to original query
			final List<String> origin = hlpr.getRessOnPredSubj(
					facetQuery.getUri(), WebLabRetrieval.IS_LINKED_TO);
			if ((origin == null) || (origin.size() != 1)) {
				this.logger
						.warn("One of the facet query ["
								+ q.getUri()
								+ "] received isn't correclty linked to the orginal query. Ignoring it.");
				continue;
			}
			// get count
			final List<String> counts = hlpr.getLitsOnPredSubj(
					facetQuery.getUri(), WebLabRetrieval.HAS_NUMBER_OF_RESULTS);
			if ((counts == null) || (counts.size() != 1)) {
				this.logger.warn("One of the facet query [" + q.getUri()
						+ "] has a bad number of results. Ignoring it.");
				continue;
			}
			int count;
			try {
				count = Integer.parseInt(counts.get(0));
			} catch (final NumberFormatException e) {
				this.logger.warn("One of the facet query [" + q.getUri()
						+ "] has a number of result which is not an integer ("
						+ counts.get(0) + "). Ignoring it.");
				continue;
			}
			// check that the facet is built upon original query
			final Set<Query> originalQuery = this
					.filterResources(receveidResultSet, new HashSet<>(
							origin), Query.class);
			if ((originalQuery == null) || (originalQuery.size() < 1)) {
				this.logger
						.warn("One of the facet query ["
								+ q.getUri()
								+ "] received isn't correclty built upon the orginial query. We will ignor it.");
				continue;
			}
			// process and extract facet query by getting scope and label
			// ugly: suppose the facet query is built by solr facet
			// suggestion: the filter query is the second one
			final Query filter = facetQuery.getQuery().get(1); // YM: Why 1
																// here? (I let
																// it but I do
																// not
																// understand
																// why...)
			if (!(filter instanceof StringQuery)) {
				this.logger.warn("One of the facet query [" + q.getUri()
						+ "] received isn't isn't a StringQuery but a ["
						+ filter.getClass().getSimpleName()
						+ "]. We will ignor it.");
				continue;
			}
			final StringQuery sfilter = (StringQuery) filter;
			final List<String> scopes = hlpr.getRessOnPredSubj(
					sfilter.getUri(), WebLabRetrieval.HAS_SCOPE);
			if ((scopes == null) || (scopes.size() != 1)) {
				this.logger
						.warn("One of the facet query ["
								+ q.getUri()
								+ "] received doesn't have a correct unique scope. We will ignor it.");
				continue;
			}

			final String scope = scopes.get(0);
			final Facet facet = new Facet(sfilter.getRequest(), count, count,
					facetQuery);
			if (!bundle.containsKey(scope)) {
				bundle.put(scope, new FacetCategory(scope, this.config
						.getFacetLabelKey().get(scope), facet));
			} else {
				bundle.get(scope).getFacets().add(facet);
			}
		}

		if (bundle.isEmpty()) {
			throw new WebLabCheckedException("Not any facet to present...");
		}

		Comparator<Facet> facetComparator = this.config.getFacetComparator();
		if (facetComparator == null) {
			facetComparator = Facet.FacetComparator.getInstance();
		}
		this.logger.debug("using facet Comparator: " + facetComparator.getClass());

		// ordering facet for presentation
		for (final FacetCategory cat : bundle.values()) {
			Collections.sort(cat.getFacets(), facetComparator);
		}

		return bundle;
	}

	@Override
	public FacetConfig getConfig() {
		return this.config;
	}

	@Override
	public void setConfig(FacetConfig config) {
		this.config = config;
	}

	protected <T extends Resource> Set<T> filterResources(
			final Resource receveidResultSet, final Set<String> acceptedURIs,
			final Class<T> resClass) {
		final Set<T> filteredQueries = new HashSet<>();
		final List<T> queries = ResourceUtil.getSelectedSubResources(
				receveidResultSet, resClass);
		for (final T q : queries) {
			if (acceptedURIs.contains(q.getUri())) {
				filteredQueries.add(q);
			}
		}
		return filteredQueries;
	}

	@Override
	public ResultSet enrichResultSet(ResultSet resultSet) {
		return resultSet;
		// TODO Auto-generated method stub

	}
}
