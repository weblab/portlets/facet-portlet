/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 * 
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 * 
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.business.bean;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.Map;

public class FacetConfig {
	public static String DEFAULT_CONF_FILE = "FacetConfigBean.xml";
	public static String DEFAULT_BEAN_NAME = "FacetConfigBean";

	/**
	 * List of properties corresponding to properties displayer as facet categories into portlet.
	 *
	 * The list order is kept when displaying categories.
	 */
	protected LinkedList<String> facetsList;

	/**
	 * Map facet properties to label to be presented
	 */
	protected Map<String, String> facetLabelKey;

	/**
	 * List of display styles for Lucene fields.
	 */
	protected Map<String, String> facetFieldsVsStylesList;

	/**
	 * List of colors used for Pie display.
	 */
	protected LinkedList<String> facetPieColorsList;

	/**
	 * FacetComparator used to order facets
	 */
	protected Comparator<Facet> facetComparator;
	
	/**
	 * Facet properties
	 */
	protected String facetClass, facetValueClass, isLinkedToFacet, facetCountProperty, facetFilterQueryProperty, facetFilterQueryOperatorProperty;
	
	

	// getters and setters
	public LinkedList<String> getFacetsList() {
		return this.facetsList;
	}

	public void setFacetsList(LinkedList<String> facetsList) {
		this.facetsList = facetsList;
	}

	public Map<String, String> getFacetLabelKey() {
		return this.facetLabelKey;
	}

	public void setFacetLabelKey(Map<String, String> facetLabelKey) {
		this.facetLabelKey = facetLabelKey;
	}

	/**
	 * @return the facetFieldsVsStylesList
	 */
	public Map<String, String> getFacetFieldsVsStylesList() {
		return this.facetFieldsVsStylesList;
	}

	/**
	 * @param facetFieldsVsStylesList
	 *            the facetFieldsVsStylesList to set
	 */
	public void setFacetFieldsVsStylesList(Map<String, String> facetFieldsVsStylesList) {
		this.facetFieldsVsStylesList = facetFieldsVsStylesList;
	}

	public LinkedList<String> getFacetPieColorsList() {
		return this.facetPieColorsList;
	}

	public void setFacetPieColorsList(LinkedList<String> facetPieColorsList) {
		this.facetPieColorsList = facetPieColorsList;
	}

	/**
	 * @return the facetClass
	 */
	public String getFacetClass() {
		return this.facetClass;
	}

	/**
	 * @param facetClass
	 *            the facetClass to set
	 */
	public void setFacetClass(String facetClass) {
		this.facetClass = facetClass;
	}

	/**
	 * @return the facetValueClass
	 */
	public String getFacetValueClass() {
		return this.facetValueClass;
	}

	/**
	 * @param facetValueClass
	 *            the facetValueClass to set
	 */
	public void setFacetValueClass(String facetValueClass) {
		this.facetValueClass = facetValueClass;
	}

	/**
	 * @return the isLinkedToFacet
	 */
	public String getIsLinkedToFacet() {
		return this.isLinkedToFacet;
	}

	/**
	 * @param isLinkedToFacet
	 *            the isLinkedToFacet to set
	 */
	public void setIsLinkedToFacet(String isLinkedToFacet) {
		this.isLinkedToFacet = isLinkedToFacet;
	}

	/**
	 * @return the facetCountProperty
	 */
	public String getFacetCountProperty() {
		return this.facetCountProperty;
	}

	/**
	 * @param facetCountProperty
	 *            the facetCountProperty to set
	 */
	public void setFacetCountProperty(String facetCountProperty) {
		this.facetCountProperty = facetCountProperty;
	}

	/**
	 * @return the facetFilterQueryProperty
	 */
	public String getFacetFilterQueryProperty() {
		return this.facetFilterQueryProperty;
	}

	/**
	 * @param facetFilterQueryProperty
	 *            the facetFilterQueryProperty to set
	 */
	public void setFacetFilterQueryProperty(String facetFilterQueryProperty) {
		this.facetFilterQueryProperty = facetFilterQueryProperty;
	}

	/**
	 * @return the facetFilterQueryOperatorProperty
	 */
	public String getFacetFilterQueryOperatorProperty() {
		return this.facetFilterQueryOperatorProperty;
	}

	/**
	 * @param facetFilterQueryOperatorProperty
	 *            the facetFilterQueryOperatorProperty to set
	 */
	public void setFacetFilterQueryOperatorProperty(String facetFilterQueryOperatorProperty) {
		this.facetFilterQueryOperatorProperty = facetFilterQueryOperatorProperty;
	}

	/**
	 * the facet comparator
	 * @return the facet comparator
	 */
	public Comparator<Facet> getFacetComparator() {
		return this.facetComparator;
	}

	/**
	 * @param facetComparator the facet Comparator to set
	 */
	public void setFacetComparator(Comparator<Facet> facetComparator) {
		this.facetComparator = facetComparator;
	}
	
	
}
