/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.business.service;

import java.text.SimpleDateFormat;

import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.model.Operator;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.core.services.searcher.SearchArgs;
import org.ow2.weblab.portlet.business.bean.FacetBundle;
import org.ow2.weblab.portlet.business.bean.FacetConfig;

public interface IFacetBusinessService {
	public enum DATES_RANGE { DAY, WEEK, MONTH}

	// Date format for SolR
	public static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");


	public SearchArgs createSearchArgForSelectedFacet(FacetBundle facets, String facetQueryURI, Operator operator);

	/**
	 * This method creates a SearchArgs according to the enumeration parameter
	 * @param facets Facet to query
	 * @param range Date range enumeration parameter DAY, WEEK or MONTH
	 * @return The query to send to the searcher
	 */
	public SearchArgs createSearchArgsForDates(FacetBundle facets, DATES_RANGE range);
	public FacetBundle processResultSet(final ResultSet receveidResultSet) throws WebLabCheckedException;
	public FacetConfig getConfig();
	public void setConfig(FacetConfig config);
	public ResultSet enrichResultSet(ResultSet resultSet) throws WebLabCheckedException;

}
