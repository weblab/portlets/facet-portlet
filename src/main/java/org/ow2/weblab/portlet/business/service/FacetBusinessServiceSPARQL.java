/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */
package org.ow2.weblab.portlet.business.service;

import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.components.client.WebLabClient;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.helper.impl.SemanticResource;
import org.ow2.weblab.core.model.ComposedQuery;
import org.ow2.weblab.core.model.Operator;
import org.ow2.weblab.core.model.Query;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.core.model.StringQuery;
import org.ow2.weblab.core.services.Analyser;
import org.ow2.weblab.core.services.analyser.ProcessArgs;
import org.ow2.weblab.core.services.searcher.SearchArgs;
import org.ow2.weblab.portlet.business.bean.Facet;
import org.ow2.weblab.portlet.business.bean.FacetBundle;
import org.ow2.weblab.portlet.business.bean.FacetCategory;
import org.ow2.weblab.portlet.business.bean.FacetConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.hp.hpl.jena.query.QuerySolution;


/**
 * The Facet portlet business service implementation
 * @author emilienbondu
 *
 */
@Service()
public class FacetBusinessServiceSPARQL implements IFacetBusinessService{

	public static final String FACET_SUGGESTION = "FacetSuggestion";
	private String SPARQL_QUERY;

	// attributes
	private Log logger = LogFactory.getLog(FacetBusinessServiceSPARQL.class);

	@Autowired
	private FacetConfig config;

	// methods
	@Override
	public SearchArgs createSearchArgForSelectedFacet(FacetBundle facets, String facetQueryURI, Operator operator) {

		//long start = System.currentTimeMillis();
		//Query facetQueryToLaunch = facets.getFQueryByURIMap().get(facetQueryURI);

		// remove annotation that tell the query came from facet suggestion
		// (it's now a query selected by user)

		ComposedQuery query = new ComposedQuery();
		query.setUri("http://facet_"+UUID.randomUUID().toString());
		query.setOperator(operator);
		query.getQuery().add(facets.getOrginalQuery());
		query.getQuery().add(facets.getFQueryByURIMap().get(facetQueryURI));

		//facetQueryToLaunch = this.prepareQuery(facetQueryToLaunch);

		// TODO launch the right event to search portlet
		// create args for search service
		final SearchArgs args = new SearchArgs();
		args.setLimit(Integer.valueOf(10));
		args.setOffset(Integer.valueOf(0));
		args.setQuery(query);

		//logger.debug("new query prepared in "+(System.currentTimeMillis() - start)+"ms");


		return args;
	}

	@Override
	public SearchArgs createSearchArgsForDates(FacetBundle facets,
			DATES_RANGE range) {
		ComposedQuery query = new ComposedQuery();
		query.setUri("http://facet_"+UUID.randomUUID().toString());
		query.setOperator(Operator.AND);
		query.getQuery().add(facets.getOrginalQuery());

		StringQuery dateQuery= new StringQuery();
		GregorianCalendar today = new GregorianCalendar();
		//Compute the end date of search
		String end = DATE_FORMAT.format(today.getTime());

		//Then reset it to midnight
		today.set(Calendar.HOUR_OF_DAY, 23);
		today.set(Calendar.MINUTE, 59);
		today.set(Calendar.SECOND, 59);
		today.set(Calendar.MILLISECOND, 0);

		//Prepare the query in SolR format
		StringBuffer queryBuffer = new StringBuffer("hasGatheringDate:[ ");

		switch(range) {
		case DAY:
			GregorianCalendar yesterday = today;
			//This way, we get data from yesterday to now
			yesterday.add(Calendar.DAY_OF_MONTH, -2);
			queryBuffer.append(DATE_FORMAT.format(yesterday.getTime()));
			break;

		case WEEK:
			GregorianCalendar lastWeek = today;
			lastWeek.add(Calendar.WEEK_OF_YEAR, -1);
			queryBuffer.append(DATE_FORMAT.format(lastWeek.getTime()));
			break;

		case MONTH:
			GregorianCalendar lastMonth = today;
			lastMonth.add(Calendar.MONTH, -1);
			queryBuffer.append(DATE_FORMAT.format(lastMonth.getTime()));
			break;
		}

		queryBuffer.append(" TO " + end + " ]");
		dateQuery.setRequest(queryBuffer.toString());
		dateQuery.setUri("http://facet_"+UUID.randomUUID().toString());

		query.getQuery().add(dateQuery);

		final SearchArgs args = new SearchArgs();
		args.setLimit(Integer.valueOf(10));
		args.setOffset(Integer.valueOf(0));
		args.setQuery(query);

		return args;
	}


	/**
	 * Processing a ResultSet to extract the facet and related information and
	 * produce a valid FacetBundle.
	 *
	 * @param receveidResultSet The received result set
	 * @return The Facet Bundle Bean
	 * @throws WebLabCheckedException If an error occurred retrieving the sparql query
	 */
	@Override
	public FacetBundle processResultSet(final ResultSet receveidResultSet) throws WebLabCheckedException {

		if (this.SPARQL_QUERY == null) {
			try {

				this.SPARQL_QUERY = FileUtils.readFileToString(new ClassPathResource("retrieveFacets.rq").getFile());
			} catch (IOException e) {
				this.logger.error("Unable to get the SPARQL file to retrieve facets"+e.getMessage());
				throw new WebLabCheckedException(e.getMessage());
			}
		}
		// getting facet queries
		long t = 0;
		if (this.logger.isDebugEnabled()) {
			this.logger.debug("Entering processResultSet SPARQL for " + receveidResultSet.getUri() + ".");
			t = System.currentTimeMillis();
		}

		SemanticResource semRes = new SemanticResource(receveidResultSet);

		//semRes.constructAsRDF("CONSTRUCT {?s ?p ?o} WHERE {?s ?p ?o}", System.out);

		com.hp.hpl.jena.query.ResultSet rs = semRes.selectAsJenaResultSet(this.SPARQL_QUERY);

		if (this.logger.isDebugEnabled()) {
			this.logger.debug("ResultSet loading and SPARQL query execution done in "+(System.currentTimeMillis()-t)+" ms");
		}

		final FacetBundle bundle = new FacetBundle();
		Map<String, Resource> resourceMap = ResourceUtil.getResourceUriMap(receveidResultSet);
		while (rs.hasNext()) {

			QuerySolution qs = rs.next();
			bundle.setOrginalQuery((Query) resourceMap.get(qs.getResource("originalQuery").getURI()));
			Query query  = (Query) resourceMap.get(qs.getResource("filterQuery").getURI());
			Facet facet = new Facet(qs.getLiteral("label").getString(), qs.getLiteral("countAnd").getInt(), qs.getLiteral("countNot").getInt(), query);

			String scope = qs.getResource("scope").getURI();
			if (!bundle.containsKey(scope)) {
				bundle.put(scope, new FacetCategory(scope, this.config.getFacetLabelKey().get(scope), facet));
			} else {
				if (!bundle.get(scope).getFacets().contains(facet)) {
					bundle.get(scope).getFacets().add(facet);
				}
			}
		}
		return bundle;
	}

	@Override
	public FacetConfig getConfig() {
		return this.config;
	}

	@Override
	public void setConfig(FacetConfig config) {
		this.config = config;
	}

	@Override
	public ResultSet enrichResultSet(ResultSet resultSet) throws WebLabCheckedException {
		Analyser facetService = WebLabClient.getAnalyser("uri_client", UUID.randomUUID().toString(), "http://weblab.ow2.org/core/1.2/services/analyser#FacetSuggestion");
		ProcessArgs args = new ProcessArgs();
		args.setResource(resultSet);
		try {
			long start = System.currentTimeMillis();
			ResultSet rs = (ResultSet) facetService.process(args).getResource();
			this.logger.debug("facets retreived from service in :"+(System.currentTimeMillis() - start));
			return rs;
		} catch (Exception e) {
			this.logger.error("Unable to get facets on service :"+e.getMessage());
			throw new WebLabCheckedException("Unable to get facets on service");
		}
	}
}
