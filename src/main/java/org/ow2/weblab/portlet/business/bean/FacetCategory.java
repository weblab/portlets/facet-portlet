/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.business.bean;

import java.util.LinkedList;
import java.util.List;

/**
 * Class that contains the multiple facets organized in a category. The category
 * being linked to a property URI and having a specific label.
 *
 * @author gdupont - WebLab team ; CASSIDIAN, an EADS company
 */
public class FacetCategory {


	protected String label;
	protected String uri;
	protected List<Facet> facets;

	public FacetCategory() {
		super();
	}

	public FacetCategory(final String uri, final String label, final Facet firstValue) {
		super();
		this.uri = uri;
		this.label = label;
		this.facets = new LinkedList<>();
		this.facets.add(firstValue);
	}

	public FacetCategory(final String uri, final String label, final List<Facet> values) {
		super();
		this.uri = uri;
		this.label = label;
		this.facets = values;
	}

	public String getLabel() {
		return this.label;
	}

	public void setLabel(final String label) {
		this.label = label;
	}

	public List<Facet> getFacets() {
		return this.facets;
	}

	public void setFacet(final List<Facet> facets) {
		this.facets = facets;
	}

	public String getUri() {
		return this.uri;
	}

	public void setUri(final String uri) {
		this.uri = uri;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + ((this.label == null) ? 0 : this.label.hashCode());
		result = (prime * result) + ((this.facets == null) ? 0 : this.facets.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		final FacetCategory other = (FacetCategory) obj;
		if (this.label == null) {
			if (other.label != null) {
				return false;
			}
		} else if (!this.label.equals(other.label)) {
			return false;
		}
		if (this.facets == null) {
			if (other.facets != null) {
				return false;
			}
		} else if (!this.facets.equals(other.facets)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "FacetBean [label=" + this.label + ", facets=" + this.facets + "]";
	}
}
