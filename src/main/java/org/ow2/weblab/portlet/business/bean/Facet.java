/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.business.bean;

import java.util.Comparator;

import org.ow2.weblab.core.model.Query;

/**
 * Simple bean that holds information linked to a facet : its label; its count
 * of results when applied as filter and the Query to be launched if selected.
 *
 * @author gdupont - WebLab team ; CASSIDIAN, an EADS company
 */
public class Facet {

	protected String label;
	protected int countAnd, countNot;
	protected String filterQuery;
	protected Query query;

	public Facet() {
		super();
	}

	public Facet(final String label, final int countAnd, final int countNot, final Query facetQuery) {
		super();
		this.label = label;
		this.countAnd = countAnd;
		this.countNot = countNot;
		this.query = facetQuery;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return this.label;
	}

	/**
	 * @param label
	 *            the label to set
	 */
	public void setLabel(final String label) {
		this.label = label.trim();
	}

	/**
	 * @return the count
	 */
	public int getCountAnd() {
		return this.countAnd;
	}

	/**
	 * @param countAnd
	 *            the count to set
	 */
	public void setCountAnd(final int countAnd) {
		this.countAnd = countAnd;
	}

	/**
	 * @param countNot
	 *            the count to set
	 */
	public void setCountNot(final int countNot) {
		this.countNot = countNot;
	}

	/**
	 * @return the count
	 */
	public int getCountNot() {
		return this.countNot;
	}


	/**
	 * @return the filterQuery
	 */
	public String getFilterQuery() {
		return this.filterQuery;
	}

	/**
	 * @param filterQuery
	 *            the filterQuery to set
	 */
	public void setFilterQuery(final String filterQuery) {
		this.filterQuery = filterQuery;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + this.countAnd;
		result = (prime * result) + this.countNot;
		result = (prime * result) + ((this.filterQuery == null) ? 0 : this.filterQuery.hashCode());
		result = (prime * result) + ((this.label == null) ? 0 : this.label.hashCode());
		return result;
	}


	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		final Facet other = (Facet) obj;
		if (this.countAnd != other.countAnd) {
			return false;
		}
		if (this.filterQuery == null) {
			if (other.filterQuery != null) {
				return false;
			}
		} else if (!this.filterQuery.equals(other.filterQuery)) {
			return false;
		}
		if (this.label == null) {
			if (other.label != null) {
				return false;
			}
		} else if (!this.label.equals(other.label)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "FacetValueBean [count=" + this.countAnd + ", filterQuery=" + this.filterQuery + ", label=" + this.label + "]";
	}

	public Query getQuery() {
		return this.query;
	}

	public void setQuery(final Query query) {
		this.query = query;
	}


	/**
	 * Default comparator based on label
	 *
	 * @author gdupont - WebLab team ; CASSIDIAN, an EADS company
	 */
	public static class FacetComparator implements Comparator<Facet> {
		public static FacetComparator getInstance() {
			return new FacetComparator();
		}

		@Override
		public int compare(final Facet o1, final Facet o2) {
			return o1.getLabel().compareTo(o2.getLabel());
		}
	}
}
