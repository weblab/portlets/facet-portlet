/**
 * WEBLAB: Service oriented integration platform for media mining and intelligence applications
 *
 * Copyright (C) 2004 - 2015 Airbus Defence and Space
 *
 * This library is free software; you can redistribute it and/or modify it under the terms of
 * the GNU Lesser General Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth
 * Floor, Boston, MA 02110-1301 USA
 */

package org.ow2.weblab.portlet.mvc.spring.controller;

import java.io.IOException;
import java.util.LinkedList;

import javax.portlet.ActionResponse;
import javax.portlet.EventRequest;
import javax.portlet.ResourceResponse;
import javax.xml.namespace.QName;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.model.Operator;
import org.ow2.weblab.core.model.ResultSet;
import org.ow2.weblab.portlet.business.bean.FacetBundle;
import org.ow2.weblab.portlet.business.bean.FacetConfig;
import org.ow2.weblab.portlet.business.service.IFacetBusinessService;
import org.ow2.weblab.portlet.business.service.IFacetBusinessService.DATES_RANGE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.EventMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

/**
 * Facet portlet controller based on the Spring
 * MVC Porltet framework. Process request for the VIEW portlet mode.
 *
 * @author emilienbondu, gdupont
 *
 */

@Controller
@RequestMapping(value="VIEW")
@SessionAttributes({"facet_cats", "received_RS", "update_facets", "reset"})
public class FacetController {

	private Log logger = LogFactory.getLog(FacetController.class);

	@Autowired
	@Qualifier("facetBusinessService")
	private IFacetBusinessService service;

	@Autowired
	private FacetConfig config;

	@ModelAttribute("facet_order")
	public LinkedList<String> getFacetList() {
		return this.config.getFacetsList();
	}

	//#############################################################################
	//					  render mapping methods								  #
	//#############################################################################
	@RenderMapping()
	public ModelAndView doView(ModelMap model) throws WebLabCheckedException {
		ModelAndView mav = new ModelAndView("view");
		mav.addAllObjects(model);
		return mav;
	}

	//#############################################################################
	//					  action mapping methods								  #
	//#############################################################################
	@ActionMapping(value="add_facet")
	public void addFacet(ActionResponse response, @RequestParam("facet_query_uri") String facetQueryURI, @ModelAttribute("facet_cats") FacetBundle facets) throws WebLabCheckedException {
		if (this.logger.isDebugEnabled()) {
			this.logger.debug("receiving action add_facet");
		}

		// launch event to broadcast the searchArgs to all portlets
		if (this.logger.isDebugEnabled()) {
			this.logger.debug("publishing searchArgs in event: [{http://weblab.ow2.org/portlet/action}addFacet]");
		}

		response.setEvent(QName.valueOf("{http://weblab.ow2.org/portlet/action}addFacet"), this.service.createSearchArgForSelectedFacet(facets, facetQueryURI, Operator.AND));
	}

	/**
	 * Method called by the JSP when clicking on the "yesterday" link
	 * @param response The action response (used to set the event to be sent)
	 * @param facets Facets received from the JSP
	 * @throws WebLabCheckedException If something wrong occurs
	 */
	@ActionMapping(value="add_day_facet")
	public void addDayFacet(ActionResponse response, @ModelAttribute("facet_cats") FacetBundle facets) throws WebLabCheckedException {
		if (this.logger.isDebugEnabled()) {
			this.logger.debug("receiving action add_day_facet");
		}

		//Launch event to broadcast the searchArgs to all portlets
		if (this.logger.isDebugEnabled()) {
			this.logger.debug("publishing searchArgs in event: [{http://weblab.ow2.org/portlet/action}addFacet]");
		}

		response.setEvent(QName.valueOf("{http://weblab.ow2.org/portlet/action}addFacet"), this.service.createSearchArgsForDates(facets, DATES_RANGE.DAY));
	}

	/**
	 * Method called by the JSP when clicking on the "last week" link
	 * @param response The action response used to set the event to be sent
	 * @param facets Facets received from the JSP
	 * @throws WebLabCheckedException If something wrong occurs
	 */
	@ActionMapping(value="add_week_facet")
	public void addWeekFacet(ActionResponse response, @ModelAttribute("facet_cats") FacetBundle facets) throws WebLabCheckedException {
		if (this.logger.isDebugEnabled()) {
			this.logger.debug("receiving action add_week_facet");
		}

		//Launch event to broadcast the searchArgs to all portlets
		if (this.logger.isDebugEnabled()) {
			this.logger.debug("publishing searchArgs in event: [{http://weblab.ow2.org/portlet/action}addFacet]");
		}

		response.setEvent(QName.valueOf("{http://weblab.ow2.org/portlet/action}addFacet"), this.service.createSearchArgsForDates(facets, DATES_RANGE.WEEK));
	}

	/**
	 * Method called by the JSP when clicking on the "last month" link
	 * @param response The action response used to set the event to be sent
	 * @param facets Facets received from the JSP
	 * @throws WebLabCheckedException If something wrong occurs
	 */
	@ActionMapping(value="add_month_facet")
	public void addMonthFacet(ActionResponse response, @ModelAttribute("facet_cats") FacetBundle facets) throws WebLabCheckedException {
		if (this.logger.isDebugEnabled()) {
			this.logger.debug("receiving action add_month_facet");
		}

		//Launch event to broadcast the searchArgs to all portlets
		if (this.logger.isDebugEnabled()) {
			this.logger.debug("publishing searchArgs in event: [{http://weblab.ow2.org/portlet/action}addFacet]");
		}

		response.setEvent(QName.valueOf("{http://weblab.ow2.org/portlet/action}addFacet"), this.service.createSearchArgsForDates(facets, DATES_RANGE.MONTH));
	}

	@ActionMapping(value="remove_facet")
	public void removeFacet(ActionResponse response, @RequestParam("facet_query_uri") String facetQueryURI, @ModelAttribute("facet_cats") FacetBundle facets) throws WebLabCheckedException {
		if (this.logger.isDebugEnabled()) {
			this.logger.debug("receiving action remove_facet");
		}

		// launch event to broadcast the searchArgs to all portlets
		if (this.logger.isDebugEnabled()) {
			this.logger.debug("publishing searchArgs in event: [{http://weblab.ow2.org/portlet/action}addFacet]");
		}

		response.setEvent(QName.valueOf("{http://weblab.ow2.org/portlet/action}addFacet"), this.service.createSearchArgForSelectedFacet(facets, facetQueryURI, Operator.NOT));
	}

	//#############################################################################
	//					  event mapping methods								      #
	//#############################################################################
	@EventMapping(value="{http://weblab.ow2.org/portlet/reaction}displayResultSet")
	public void displayResutlSet(EventRequest request, ModelMap model, SessionStatus status) throws WebLabCheckedException, IOException {
		if (this.logger.isDebugEnabled()) {
			this.logger.debug("Reaction: Display resultSet.");
		}

		if ((request.getEvent().getValue() == null) || !(request.getEvent().getValue() instanceof ResultSet)) {
			this.logger.error("The Event passed has no (or incorrect) value: [" + request.getEvent().getValue() + "]");
		} else {

			ResultSet receveidResultSet = (ResultSet) request.getEvent().getValue();
			// Logging resultSet
			if (this.logger.isTraceEnabled()) {
				try {
					final String resXML = ResourceUtil.saveToXMLString(receveidResultSet);
					this.logger.trace("Event [Display ResultSet] received with ResultSet [" + resXML + "].");
				} catch (final WebLabCheckedException wlce) {
					this.logger.warn("Unable to marshallthe resultset received for Event [Display ResultSet] ", wlce);
				}
			} else {
				this.logger.info("Event [Display ResultSet] received with ResultSet [" + receveidResultSet.getUri() + "].");
			}

			if (receveidResultSet == null || receveidResultSet.getPok() == null || receveidResultSet.getResource().isEmpty()) {
				this.logger.trace("ResultSet received is null: do reset.");

				model.addAttribute("reset", Boolean.TRUE);

			} else {
				// update model

				long t = System.currentTimeMillis();
				//model.addAttribute("facet_cats", service.processResultSet(((ResultSet)request.getEvent().getValue())));
				model.addAttribute("received_RS", request.getEvent().getValue());
				model.addAttribute("update_facets", Boolean.TRUE);

				this.logger.info("facets retrieved in "+(System.currentTimeMillis()-t)+" ms");
			}
		}
	}

	@EventMapping(value="{http://weblab.ow2.org/portlet/reaction}reset")
	public void reset(ModelMap model) throws WebLabCheckedException {
		this.logger.info("Reaction: Resetting facets.");
		model.addAttribute("reset", Boolean.TRUE);
	}


	//#############################################################################
	//					  resources handlers									  #
	//#############################################################################
	@ResourceMapping("facets")
	public ModelAndView serveFacets(ModelMap model, ResourceResponse response) throws WebLabCheckedException {
		response.setContentType("text/html");
	    response.setCharacterEncoding("UTF-8");

		if ((model.containsAttribute("reset") && ((Boolean) model.get("reset")).booleanValue()) || !model.containsAttribute("facet_cats")) {
			model.addAttribute("facet_cats", new FacetBundle());
			model.addAttribute("reset", Boolean.FALSE);
		}

		if (model.containsAttribute("update_facets") && ((Boolean)model.get("update_facets")).booleanValue()) {
			this.logger.info("updating facets");
			model.addAttribute("received_RS", this.service.enrichResultSet(((ResultSet)model.get("received_RS"))));
			model.addAttribute("facet_cats", this.service.processResultSet(((ResultSet)model.get("received_RS"))));
			model.remove("received_RS");
			model.addAttribute("update_facets", Boolean.FALSE);
		}

		ModelAndView mav = new ModelAndView("facets");
		mav.addAllObjects(model);
		return mav;

	}

	//#############################################################################
	//					  exceptions handlers									  #
	//#############################################################################
	@ExceptionHandler(WebLabCheckedException.class)
	public ModelAndView handleException(WebLabCheckedException ex) {
		ModelAndView mav = new ModelAndView("view");
		mav.addObject("error", Boolean.TRUE);
		mav.addObject("error_message", ex.getMessage());
		return mav;
	}

}
